/** @format */

// use only for mysql2
// const mysql = require("mysql2");
// const pool = mysql.createPool({
//   host: "localhost",
//   user: "root",
//   database: "nodejs-project",
//   password: "123@123a",
// });

// module.exports = pool.promise();

// use mysql2 and sequelize
const Sequelize = require("sequelize");

const sequelize = new Sequelize("nodejs-project", "root", "123@123a", {
  dialect: "mysql",
  host: "localhost",
});
module.exports = sequelize;
