/** @format */

//use sequelize

const Sequelize = require("sequelize");
const sequelize = require("../util/database");

const cartItem = sequelize.define("cartItem", {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true,
  },
  quantity: { type: Sequelize.INTEGER },
});
module.exports = cartItem;
