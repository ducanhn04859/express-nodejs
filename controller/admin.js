/** @format */

const { render } = require("pug");
const Product = require("../models/product");

exports.getAddProduct = (req, res, next) => {
  res.render("admin/edit-products", {
    pageTitle: "Add Product",
    path: "/admin/add-product",
    formsCSS: true,
    productCSS: true,
    activeAddProduct: true,
    editing: false,
  });
};

exports.postAddProduct = (req, res, next) => {
  const title = req.body.title;
  const ImageURL = req.body.ImageURL;
  const price = req.body.price;
  const description = req.body.description;
  // use only with mysql2
  // const product = new Products(null, title, ImageURL, description, price);
  // product
  //   .save()
  //   .then(() => {
  //     res.redirect("/");
  //   })
  //   .catch((err) => {
  //     console.log(err);
  //   });

  //use with mysql2 and sequelize
  req.user
    .createProduct({
      title: title,
      imageURL: ImageURL,
      price: price,
      description: description,
    })
    .then((result) => {
      console.log(result);
      console.log("CREATE PRODUCT DONE !");
      res.redirect("/admin/products");
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getEditProduct = (req, res, next) => {
  const editMode = req.query.edit;
  if (editMode != "true") {
    res.redirect("/");
  } else {
    const productId = req.params.productId;
    // use with mysql2
    // Product.findById(productId, (product) => {
    //   if (!product) {
    //     res.redirect("/error");
    //   } else {
    //     res.render("admin/edit-products", {
    //       pageTitle: "Edit Product",
    //       path: "/admin/edit-product",
    //       editing: editMode,
    //       product: product,
    //     });
    //   }
    // });
    // use with mysql2 and sequelize
    req.user
      .getProducts({ where: { id: productId } })
      // .Product.findByPk(productId)
      .then((products) => {
        const product = products[0];
        res.render("admin/edit-products", {
          pageTitle: "Edit Product",
          path: "/admin/edit-product",
          editing: editMode,
          product: product,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }
};

exports.postEditProduct = (req, res, next) => {
  const productId = req.body.productId;
  const updateTitle = req.body.title;
  const updateImageURL = req.body.imageUrl;
  const updatePrice = req.body.price;
  const updateDescription = req.body.description;
  // use with mysql2
  // const updateProduct = new Products(
  //   productId,
  //   updateTitle,
  //   updateImageURL,
  //   updateDescription,
  //   updatePrice
  // );
  // updateProduct.save();
  // use with mysql2 and sequelize
  Product.findByPk(productId)
    .then((product) => {
      product.title = updateTitle;
      product.price = updatePrice;
      product.description = updateDescription;
      product.ImageURL = updateImageURL;
      return product.save();
    })
    .then((result) => {
      console.log("UPDATE PRODUCT DONE !");
      res.redirect("/admin/products");
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getProducts = (req, res, next) => {
  // use with mysql2
  // Product.fetchAll((products) => {
  //   res.render("admin/products", {
  //     prods: products,
  //     pageTitle: "Admin Products",
  //     path: "/admin/products",
  //   });
  // });
  // use with mysql2 and sequelize
  req.user
    .getProducts()
    // Product.findAll()
    .then((products) => {
      res.render("admin/products", {
        prods: products,
        pageTitle: "Admin Products",
        path: "/admin/products",
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.postDeleteProducts = (req, res, next) => {
  const productId = req.body.productId;
  // use with mysql2 and sequelize
  // Product.deleteById(productId);
  // use with mysql2 and sequelize
  Product.findByPk(productId)
    .then((product) => {
      return product.destroy();
    })
    .then((result) => {
      console.log("DELETE PRODUCT DONE !");
      res.redirect("/admin/products");
    })
    .catch((err) => {
      console.log(err);
    });
};
